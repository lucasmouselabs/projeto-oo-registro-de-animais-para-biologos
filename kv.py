from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.textinput import TextInput
from kivy.network.urlrequest import UrlRequest
from urllib import urlencode
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.uix.camera import Camera
from kivy.lang import Builder
from kivy.uix.label import Label
from kivy.lang import Builder
from kivy.graphics import Color, Rectangle
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.image import AsyncImage

AllUser = {}
dicUsuario =  {}

Builder.load_string("""
<StartScreen>:
	name: 		"startscreen"
	FloatLayout:
		canvas.before:
			Color:
				rgba: 1, 1, 1, 1
			Rectangle:
				pos: self.pos
				size: self.size
		Button:
			background_normal: 	"FODA.png"
			size_hint:		.4, .8
			pos_hint:		{"x": .3, "y": .2}
			on_press:		root.manager.current = "mainscreen"

<MainScreen>:	
	name:					"mainscreen"
	FloatLayout:
		canvas.before:
			Color:
				rgba: 0.1, 0.8, 0.1, 1
			Rectangle:
				pos: self.pos
				size: self.size

		Button:
			text:			"Camera"
			size_hint:		.2, .2
			pos_hint:		{"x": .4, "y": .2}
			on_press:		root.manager.current = "camerascreen"
		Button:
			text:			"Login"
			size_hint:		.2, .2
			pos_hint:		{"x": .4, "y": .6}
			on_press:		root.manager.current = "loginscreen"
			background_normal: 	

<CameraScreen>:
	name:					"camerascreen"
	FloatLayout:
		Camera:
			play:			True
		Button:
			text:			"Main"
			size_hint:		.1, .1
			pos_hint:		{"x": 0, "y": 0}
			on_press:		root.manager.current = "mainscreen"

<LoginScreen>:
	name:					"loginscreen"
	FloatLayout:
		canvas.before:
			Color:
				rgba: 0.1, 0.8, 0.1, 1
			Rectangle:
				pos: self.pos
				size: self.size
		Label:
			text:			"Usuario"
			size_hint:		.1, .1
			pos_hint:		{"x": .45, "y": .7}
		TextInput:
			id:			username
			size_hint:		.4, .1
			pos_hint:		{"x": .3, "y": .6}
		Label:
			text:			"Senha"
			size_hint:		.1, .1
			pos_hint:		{"x": .45, "y": .4}
		TextInput:
			id:			password
			password:		True
			size_hint:		.4, .1
			pos_hint:		{"x": .3, "y": .3}
		Button:
			text:			"Login"
			size_hint:		.2, .1
			pos_hint:		{"x": .6, "y": .1}
			#on_press:		root.send(username.text, password.text)
			#Devia ir para PerfilScreen somente quando validasse 
			on_press:		root.manager.current = "perfilscreen"
		Button:
			text:			"Cadastrar"
			size_hint:		.2, .1
			pos_hint:		{"x": .2, "y": .1}	
			on_press:		root.manager.current = "cadastroscreen"
		Button:
			text:			"Main"
			size_hint:		.1, .1
			pos_hint:		{"x": 0, "y": 0}
			on_press:		root.manager.current = "mainscreen"

<PerfilScreen>:
	name:					"perfilscreen"
	FloatLayout:
		canvas.before:
			Color:
				rgba: 0.1, 0.8, 0.1, 1
			Rectangle:
				pos: self.pos
				size: self.size
		Button:

			size_hint:		.2, .4
			pos_hint:		{"x": .1, "y": .5}
			background_normal: "image.jpg"
		Label:
		#	PEGAR O NOME DO USUARIO
			
			text: 	"LUCAS RIBEIRO"
			font_size:		'40sp'
			size_hint:		.5, .2
			pos_hint: 		{"x": .3, "y": .8}
		Button:
			text:			"Login"
			size_hint:		.1, .1
			pos_hint:		{"x": 0, "y": 0}
			on_press:		root.manager.current = "loginscreen"
		
		Button:
			id:			Lista de Animais
			text: 		"Lista de Animais"
			size_hint:		.4, .1
			pos_hint:		{"x": .4, "y": .6}
			on_press: 		root.manager.current = "listascreen"
		Button:			
			id:			Cadastrar Animal
			text: 		"Cadastrar Animal"
			password:		True
			size_hint:		.4, .1
			pos_hint:		{"x": .4, "y": .3}
			on_press: 		root.manager.current = "camerascreen"

<ListaScreen>
	name: 					"listascreen"
	FloatLayout:
		canvas.before:
			Color:
				rgba: 0.1, 0.8, 0.1, 1
			Rectangle:
				pos: self.pos
				size: self.size	
		Button:
			text:			"Main"
			size_hint:		.1, .1
			pos_hint:		{"x": 0, "y": 0}
			on_press:		root.manager.current = "perfilscreen"	

<CadastroScreen>
	name:					"cadastroscreen"
	FloatLayout:
		canvas.before:
			Color:
				rgba: 0.1, 0.8, 0.1, 1
			Rectangle:
				pos: self.pos
				size: self.size
		Label:
			text:			"Nick"
			size_hint:		.1, .1
			pos_hint:		{"x": .45, "y": .7}
		TextInput:
			id:			Usuario
			size_hint:		.4, .1
			pos_hint:		{"x": .3, "y": .6}
		Label:
			text:			"Senha"
			size_hint:		.1, .1
			pos_hint:		{"x": .45, "y": .4}
		TextInput:
			id:			password
			password:		True
			size_hint:		.4, .1
			pos_hint:		{"x": .3, "y": .3}
		Button:
			text:			"Cadastrar"
			size_hint:		.4, .1
			pos_hint:		{"x": .3, "y": .1}
			on_press:		root.novoContato(Usuario.text, password.text)
			on_press:		root.manager.current = "loginscreen"

<Sm>:
	StartScreen:
	MainScreen:
	CameraScreen:
	LoginScreen:
	PerfilScreen:
	ListaScreen:
	CadastroScreen:
	
""")


class Test(App):

	def build(self):
		return Sm()

class Sm(ScreenManager):
	pass

class StartScreen(Screen):
	pass

class MainScreen(Screen):
	pass

class CameraScreen(Screen):
	pass

class LoginScreen(Screen):
	
	
	def send(self, username, password):
		UrlRequest(
			"http://127.0.0.1:8080/login",
			req_body = urlencode({"username": username, "password": password}),
				on_success = self.success
		)
	def success(self, request, result):
		if result["response"] == True:
			root.manager.current()	
		
	
class PerfilScreen(Screen):
	pass

class ListaScreen(Screen):
	pass

class CadastroScreen(Screen):
	
	def novoContato(self, nome, senha, ListadeAnimais = []):
		
		dicUsuario['nome'] = nome
		dicUsuario['senha'] = senha
		dicUsuario['animais'] = ListadeAnimais
		
	print (dicUsuario)	

	print (dicUsuario)
Test().run()

