from tornado.web import RequestHandler, Application
from tornado.ioloop import IOLoop
from durus.connection import Connection
from durus.client_storage import ClientStorage
conn = Connection(ClientStorage("127.0.0.1", 8081))
root = conn.get_root()
class Login(RequestHandler):
	def post(self):
		d = {}
		for x in self.request.body.decode("utf-8").split("&"):
			y = x.split("=")
			d[y[0]] = y[1]
		print(d.items())
		if root["users"][d["username"]]["password"] == d["password"]: self.write({"response": True})
Application([(r"/login", Login)], debug = True).listen(8080)
IOLoop.instance().start()
